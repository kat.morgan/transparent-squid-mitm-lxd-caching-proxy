Source: squid3
Section: web
Priority: optional
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
XSBC-Original-Maintainer: Luigi Gangitano <luigi@debian.org>
Homepage: http://www.squid-cache.org
Standards-Version: 3.9.6
Xs-Vcs-Git: git://anonscm.debian.org/pkg-squid/pkg-squid3.git/
Build-Depends: libldap2-dev, libpam0g-dev, libdb-dev, cdbs, libsasl2-dev, debhelper (>=5), libcppunit-dev, libkrb5-dev, comerr-dev, libcap2-dev [linux-any], libecap3-dev (>= 1.0.1-2), libexpat1-dev, libxml2-dev, autotools-dev, libltdl-dev, dpkg-dev (>= 1.16.1~), pkg-config, libnetfilter-conntrack-dev [linux-any], nettle-dev, libgnutls28-dev, dh-apparmor, lsb-release, dh-autoreconf

Package: squid3
Architecture: all
Section: oldlibs
Priority: extra
Pre-Depends: squid (>= ${source:Version})
Depends: ${misc:Depends}
Description: Dummy transitional package.
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.
 .
 This is a dummy transitional package used to migrate from squid3 to squid.

Package: squid
Architecture: any
Pre-Depends: adduser
Depends: ${shlibs:Depends}, ${misc:Depends}, netbase, logrotate (>= 3.5.4-1), squid-common (= ${source:Version}), lsb-base, ssl-cert, init-system-helpers (>> 1.22ubuntu5)
Suggests: squidclient, squid-cgi, squid-purge, resolvconf (>= 0.40), smbclient, ufw, winbindd, apparmor
Breaks: squid3 (<< 3.5.12-1ubuntu1~), ufw (<< 0.35-0ubuntu2~)
Replaces: squid3 (<< 3.5.12-1ubuntu1~)
Description: Full featured Web Proxy cache (HTTP proxy)
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.

Package: squid-dbg
Architecture: any
Section: debug
Priority: extra
Depends: squid (= ${binary:Version}), ${misc:Depends}
Description: Full featured Web Proxy cache (HTTP proxy) - Debug symbols
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.
 .
 This package contains debugging symbols for binaries in squid.

Package: squid-common
Architecture: all
Depends: ${misc:Depends}, squid-langpack (>= 20110214-1)
Provides: squid3-common
Conflicts: squid3-common
Replaces: squid3-common
Description: Full featured Web Proxy cache (HTTP proxy) - common files
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.
 .
 This package contains common files (MIB and icons)

Package: squidclient
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: squid3-client
Conflicts: squid3-client
Replaces: squid3-client
Description: Full featured Web Proxy cache (HTTP proxy) - control utility
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.
 .
 This package contains a small utility that can be used to get URLs from the 
 command line.

Package: squid-cgi
Architecture: any
Depends: apache2 | httpd, ${shlibs:Depends}, ${misc:Depends}
Provides: squid3-cgi
Conflicts: squid3-cgi
Replaces: squid3-cgi
Description: Full featured Web Proxy cache (HTTP proxy) - control CGI
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.
 .
 This package contains a CGI program that can be used to query and administrate 
 a `squid' proxy cache through a web browser.
 
Package: squid-purge
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Full featured Web Proxy cache (HTTP proxy) - control utility
 Squid is a high-performance proxy caching server for web clients, supporting
 FTP, gopher, ICY and HTTP data objects.
 .
 Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
 new features including ICAP and ESI support.
 .
 This package contains a small utility that can be used to manage the disk cache
 from the command line.
