-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: squid3
Binary: squid3, squid, squid-dbg, squid-common, squidclient, squid-cgi, squid-purge
Architecture: any all
Version: 3.5.12-1ubuntu7.4
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Homepage: http://www.squid-cache.org
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-squid/pkg-squid3.git/
Testsuite: autopkgtest
Build-Depends: libldap2-dev, libpam0g-dev, libdb-dev, cdbs, libsasl2-dev, debhelper (>= 5), libcppunit-dev, libkrb5-dev, comerr-dev, libcap2-dev [linux-any], libecap3-dev (>= 1.0.1-2), libexpat1-dev, libxml2-dev, autotools-dev, libltdl-dev, dpkg-dev (>= 1.16.1~), pkg-config, libnetfilter-conntrack-dev [linux-any], nettle-dev, libgnutls28-dev, dh-apparmor, lsb-release, dh-autoreconf
Package-List:
 squid deb web optional arch=any
 squid-cgi deb web optional arch=any
 squid-common deb web optional arch=all
 squid-dbg deb debug extra arch=any
 squid-purge deb web optional arch=any
 squid3 deb oldlibs extra arch=all
 squidclient deb web optional arch=any
Checksums-Sha1:
 17b76aeb29066e4b51e493b109e1ffe98dd81d90 4773268 squid3_3.5.12.orig.tar.gz
 8d540bcdc95ec44dcc764cebc35b9e7a64b044f3 53236 squid3_3.5.12-1ubuntu7.4.debian.tar.xz
Checksums-Sha256:
 9114a2d52905761d12550d8d6e55c855deae0dee1bcc5bf45458c4a68ff9afd5 4773268 squid3_3.5.12.orig.tar.gz
 ba5fd6e58f2d2e381e3ce4b18ac97cfb51d34d4d57b88b60b237f71bc68fbcdb 53236 squid3_3.5.12-1ubuntu7.4.debian.tar.xz
Files:
 7ba7ced7fa4b7639474c7a72c69c041e 4773268 squid3_3.5.12.orig.tar.gz
 cb53c57ba584eb8bcdad0bcddeff92fb 53236 squid3_3.5.12-1ubuntu7.4.debian.tar.xz
Original-Maintainer: Luigi Gangitano <luigi@debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCgAGBQJZd1WVAAoJEOVkucJ1vdUuVtwQAJXvj7irhdumfWelBg/BiZwZ
JHEoLd+pHvo2RFqASuzb8g0TGukvqqYMv1zXOfkU95qdX2PUGt3upYjpaf+rOig4
rVGqZxLdt05hCoHQud4eJOLrATXKCVssM3rUU+EWRcFeAN4+QF4Ah+5k5UvFOi8h
hS7ZJgD3T1HG8pWOHbvoNGybd3K4TW8gmx9S+L1ev/CUFtrXuHEFzW0V5LZ4dzwp
rkpEE7alMsh9Xi3uhlaoLTawvA0PCCv83YxvI+GhylXDfNRPqh/iONAmR/4u9aFl
TJkpqjJxlxIX/NtFyU2fZrGUAAtDR5LV3GABNoUneF1pKhTeCKzwNObH69PN+Bjm
UCydGFooLV46P/Nk4dCgmMO9QfDsNREbRC3FrjMegS0mv+M0tyamMQ9XZYEQC8cP
e8+Pym4Ebg3M2kndGZoUZnvvnW0wz3dGYtCG6jj6LGqh7MKeybE8bSLsXiUjx1WL
A+GejRehc04N//CKyIrF8lkBrKiMoLn3oipmVMF+Qdq0uCvVMbEl8X7qZDNLW0Dq
YGBDdseWWrzoPaRmGNrYlIwo/nfjxrxgYXHpRmL1Qk4vUQHCtt5ZtdL0516UD7qm
RATjSs9qkXleStuRfJg7nJLnwJFJ/ofPLBZ2n55wxEMJRWiA79mH796htKaMMQnK
GWjO1SCUqaIjWpUm4vMN
=ScwH
-----END PGP SIGNATURE-----
