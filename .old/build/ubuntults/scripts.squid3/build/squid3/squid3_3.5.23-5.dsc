-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: squid3
Binary: squid3, squid, squid-dbg, squid-common, squidclient, squid-cgi, squid-purge
Architecture: any all
Version: 3.5.23-5
Maintainer: Luigi Gangitano <luigi@debian.org>
Uploaders: Santiago Garcia Mantinan <manty@debian.org>
Homepage: http://www.squid-cache.org
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/git/pkg-squid/pkg-squid3.git/
Vcs-Git: git://anonscm.debian.org/pkg-squid/pkg-squid3.git/
Testsuite: autopkgtest
Testsuite-Triggers: fakeroot
Build-Depends: libldap2-dev, libpam0g-dev, libdb-dev, cdbs, libsasl2-dev, debhelper (>= 10), libcppunit-dev, libkrb5-dev, comerr-dev, libcap2-dev [linux-any], libecap3-dev (>= 1.0.1-2), libexpat1-dev, libxml2-dev, autotools-dev, libltdl-dev, dpkg-dev (>= 1.16.1~), pkg-config, libnetfilter-conntrack-dev [linux-any], nettle-dev, libgnutls28-dev, lsb-release
Package-List:
 squid deb web optional arch=any
 squid-cgi deb web optional arch=any
 squid-common deb web optional arch=all
 squid-dbg deb debug extra arch=any
 squid-purge deb web optional arch=any
 squid3 deb oldlibs extra arch=all
 squidclient deb web optional arch=any
Checksums-Sha1:
 6b0b2091896e7874024e5f1e28eeccb0acd7e962 4730792 squid3_3.5.23.orig.tar.gz
 13ff89733b9c37fd8f65f27384984bf4514e1c54 26428 squid3_3.5.23-5.debian.tar.xz
Checksums-Sha256:
 f81eeee0fb046ad636566b51fe4f72b8bc66d454d7082ef38e273c3f4b09f6db 4730792 squid3_3.5.23.orig.tar.gz
 e54e47b2f0d250e789b3b4687ae083be5c533ef3637dcd19bf07a6aea6dd8ba2 26428 squid3_3.5.23-5.debian.tar.xz
Files:
 49d790ddee8c611ee2992e66eb8e9ae9 4730792 squid3_3.5.23.orig.tar.gz
 4a85a1bc86514ab229b3af18f04a3549 26428 squid3_3.5.23-5.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEBqPldg9hG0uxqQ5ouGiMo9h21aMFAlkz52AACgkQuGiMo9h2
1aP+WA//RyqviRqblI95X4pyDQN9gPfC7mTUjAysq8TI0Vsrv68Lh7xYUCCfNEWv
fsSq1iyFuOjuouTfc2oDxBiT4wI4thxbhAv4G/RMXEVsVypOvC9oFO/eXaYOeM0a
mORJJWyv8FLIjtcpnSvtS6Oh3DjGOTeeAtHaXfantl9+t6g52CJVkuE2J2trH62k
kvavjEldCTpL0U3ZEEjhB81dMrUdJ1hTMuuA3xSI2ofaCt/N5WOK0JN8taYqgZCZ
fX4PuQeS9CYqX1O1gJhV/i4DlMSx1AEKyXX2Ed6cjsseUuCFrPgTV0dve+X7F6aA
bkZzmCkjwhYZmcVU/rezoYaDc3iCeqrxhdffTFMg37cIoZVsBdlmi05nZsv0CHT5
nU+2os4UAZTY4T3PTrlcnZeC+74j1f0zjSO1/DLU8hUhXbpBqsEwtxDyFmGMco0E
hnQL+Lwan8lLKWhg3d4PlC8J5FdCBDJrAsgdfnDTUp5G++YRqUU8ZnsEOn37e+3U
tMbXfUVPkt1McIoAgbe1qkGe0r5nP/QpMVMYHp6elw1Nqdl7n9ojHdcghOnGhFdR
yPCA/eVaVal1qyehibBwdzoFA8kfDhSDDda5esZDMyI+hF110aovl8GXQ9cxAx00
j5Lq2ornl6GBSwzGYN7XaAO/jLNPsWvq//C8CHRFkFkFypj1h1s=
=XKYe
-----END PGP SIGNATURE-----
