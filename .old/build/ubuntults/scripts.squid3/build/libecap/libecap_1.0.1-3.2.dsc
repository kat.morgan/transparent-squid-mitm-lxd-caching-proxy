-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: libecap
Binary: libecap3-dev, libecap3
Architecture: any
Version: 1.0.1-3.2
Maintainer: Luigi Gangitano <luigi@debian.org>
Homepage: http://www.e-cap.org/Downloads
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9), autotools-dev, cdbs, dh-autoreconf
Package-List:
 libecap3 deb libs extra arch=any
 libecap3-dev deb libdevel extra arch=any
Checksums-Sha1:
 8e4924f35164e26a0633eedf7eaf0f3a45fed77b 338972 libecap_1.0.1.orig.tar.gz
 930334ca26ab02e2c94e46bee13316233e08c65a 4892 libecap_1.0.1-3.2.debian.tar.xz
Checksums-Sha256:
 675f1a40a74a103864cb771ff30162bcdd1caeef4c808799a8293a9e9fe57d8c 338972 libecap_1.0.1.orig.tar.gz
 a1eae683f4aed2dd3278ef48a0edcbf142d41ae2732a7cedc23bd2e0d384c0ae 4892 libecap_1.0.1-3.2.debian.tar.xz
Files:
 7e65d1215fd8d8609b198760fda0c264 338972 libecap_1.0.1.orig.tar.gz
 e71ed4d517993fd36fd5a2c7040eaee0 4892 libecap_1.0.1-3.2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQItBAEBCAAXBQJX9HHWEBxkb2tvQGRlYmlhbi5vcmcACgkQvX6qYHePpvWqMQ/6
A6zzC0yRnSGZmr+uOBkTRk1iuFSZLzoDmEH7ElneeuGXjT0PZAs+5dygyENo4+Dm
72+ha7fUxW23vdq0pS/zWoPkhjtSy5p0ziSvEyRcQfTmr1RSbqJXlUd6obTDFl99
tMQIHyhHas3SFZCMfFQKRaWv2X61dsSIdgRtN16FjFLFd9Ny238D8V/4zDg2q2wZ
1ud8jDS01sE37KmhMbOrA88ulR0RGE5TpKFVbnPVTR0PKgilxcEcoFWHU4sBmLid
xYqLtWbHgcII/cSVQpvpjm3GaP25rWdgeOzJXO+1hqVQRqpbVULAms9rte1Yf+Ar
0hCrpoDUpsDkuCG4JSVFqYOs9CmJ+iYMDh7+AYvhaWr1BmkI6y2vCC/ofcp15ihr
oE2GRUkdwAz49DOQ6JgzvvVru3mpg51Ag6VWWPW7zAMJ7lLSdqpFNS18KeP/abfG
xv/Vr66h+wQGX//ZjVN4uvWXaDO3EuK18sv0Ddo8EOdRbDNYZ+jCHp+mtAkrYmBv
Yq0D8OAL4z6RHhNpaE7RbYKDwPOPznSNxUSAk3wNWJecVvWSv2b5+Pt293JaG+yy
eQFRD4NQatCkByDsjHPOpb+bAzvsQaOIErFWZje1GMgw8ThZ12msg4zivHbpyM2t
PE06TQR7wjZKNmWBPHPDHCdE5vVcW6xYsW0v3j+e79M=
=lX9U
-----END PGP SIGNATURE-----
