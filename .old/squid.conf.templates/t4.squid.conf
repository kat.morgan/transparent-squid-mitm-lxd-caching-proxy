# Rules for transparent proxying
iptables -N NO_PROXY -t nat
iptables -A NO_PROXY -t nat -d 0.0.0.0/8 -j ACCEPT
iptables -A NO_PROXY -t nat -d 10.0.0.0/8 -j ACCEPT
iptables -A NO_PROXY -t nat -d 127.0.0.0/8 -j ACCEPT
iptables -A NO_PROXY -t nat -d 169.254.0.0/16 -j ACCEPT
iptables -A NO_PROXY -t nat -d 172.16.0.0/12 -j ACCEPT
iptables -A NO_PROXY -t nat -d 192.168.0.0/16 -j ACCEPT
iptables -A NO_PROXY -t nat -d 224.0.0.0/4 -j ACCEPT
iptables -A NO_PROXY -t nat -d 240.0.0.0/4 -j ACCEPT
iptables -A NO_PROXY -t nat -j RETURN
iptables -A PREROUTING -t nat -p tcp --dport 80 -j NO_PROXY
iptables -A PREROUTING -t nat -p tcp --dport 80 -j REDIRECT --to-port 3129
iptables -A PREROUTING -t nat -p tcp --dport 443 -j NO_PROXY
iptables -A PREROUTING -t nat -p tcp --dport 443 -j REDIRECT --to-ports 3130

# http://www.spinics.net/lists/squid/msg77150.html
ssl_bump splice all
sslproxy_cert_error allow all
sslproxy_flags DONT_VERIFY_PEER

# Squid normally listens to port 3128
http_port 3128
http_port 3129 intercept
https_port 3130 intercept ssl-bump generate-host-certificates=on dynamic_cert_mem_cache_size=4MB cert=/etc/squid/ssl/ca.crt key=/etc/squid/ssl/ca.key

sslcrtd_program /usr/lib64/squid/ssl_crtd -s /var/lib/ssl_db -M 4MB
sslcrtd_children 8 startup=1 idle=1

# Leave coredumps in the first cache dir
coredump_dir /var/spool/squid

never_direct allow all
cache_peer <proxt=y> parent <port> 0 no-query no-digest default

# General
http_port 3130
http_port 3128 intercept
https_port 3129 intercept ssl-bump cert=/etc/squid/ssl_cert/srtpl.pem generate-host-certificates=on dynamic_cert_mem_cache_size=4MB


acl DiscoverSNIHost at_step SslBump1
acl NoSSLIntercept ssl::server_name_regex -i "/etc/squid/url.nobump"
ssl_bump splice NoSSLIntercept
ssl_bump peek DiscoverSNIHost
ssl_bump bump all


sslproxy_cafile /usr/local/share/ca-certificates/srtpl.crt

visible_hostname Proxy
forwarded_for delete
via off
cache_mem 2048 MB

# Log

#logformat squid %tl.%03tu %6tr %>a %Ss/%03>Hs %<st %rm %ru %[un %Sh/%<a %mt
access_log /var/log/squid/access.log squid

# Cache

coredump_dir /var/spool/squid

maximum_object_size 2048000000 bytes
cache_dir aufs /var/cache/squid 256000 128 256 max-size=2048000000
quick_abort_min -1


refresh_pattern -i dl-ssl.google.com 525600 100% 525601 reload-into-ims override-expire
refresh_pattern -i https\:\/\/dl.google.com\/android\/repository/.*\.(tar|zip|deb|exe) 525600 100% 525601  reload-into-ims override-expire
refresh_pattern -i dl\.google\.com\/.*\.(zip|tar)  525600 100% 525601 reload-into-ims override-expire
refresh_pattern -i dl.genymotion.com/.*\.(bin|ova)  525600 100% 525601 reload-into-ims override-expire
refresh_pattern -i edelivery.oracle.com 5259487 20% 5259487 reload-into-ims override-expire
refresh_pattern -i downloads.gradle.org/.*\.(zip) 525600 20% 525601 reload-into-ims override-expire

#refresh_pattern http://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_i386.deb 129600 100% 129600 reload-into-ims
#refresh_pattern dl-ssl.google.com\/dl\/linux\/direct/.*\(.deb|.zip) 43200 80% 129600 reload-into-ims override-lastmod ignore-no-store refresh-ims store-stale 
#refresh_pattern ^http:\/\/dl-ssl.google.com.*\.(deb|zip)  43200 80% 129600 reload-into-ims
#refresh_pattern dl.google.com\/.*\.(deb)  129600 100% 129600 reload-into-ims
#refresh_pattern dl-ssl.google.com\/.*\.(deb)  129600 100% 129600 reload-into-ims

refresh_pattern -i appldnld\.apple\.com 129600 100% 129600 ignore-reload ignore-no-store override-expire override-lastmod 
refresh_pattern -i phobos\.apple\.com 129600 100% 129600 ignore-reload ignore-no-store override-expire override-lastmod 
refresh_pattern -i windowsupdate.com/.*\.(cab|exe|ms[i|u|f|p]|[ap]sf|wm[v|a]|dat|zip|psf) 43200 80% 129600 reload-into-ims
refresh_pattern -i microsoft.com/.*\.(cab|exe|ms[i|u|f|p]|[ap]sf|wm[v|a]|dat|zip|psf) 43200 80% 129600 reload-into-ims
refresh_pattern -i windows.com/.*\.(cab|exe|ms[i|u|f|p]|[ap]sf|wm[v|a]|dat|zip|psf) 43200 80% 129600 reload-into-ims
refresh_pattern -i microsoft.com.akadns.net/.*\.(cab|exe|ms[i|u|f|p]|[ap]sf|wm[v|a]|dat|zip|psf) 43200 80% 129600 reload-into-ims
refresh_pattern -i deploy.akamaitechnologies.com/.*\.(cab|exe|ms[i|u|f|p]|[ap]sf|wm[v|a]|dat|zip|psf) 43200 80% 129600 reload-into-ims
refresh_pattern -i download.oracle.com 5259487 20% 5259487 override-expire override-lastmod  reload-into-ims
refresh_pattern -i packagecloud-prod.global.ssl.fastly.net 129600 20% 229600  reload-into-ims
refresh_pattern -i nodejs.org/.*\.(tar|xz) 129600 20% 229600 reload-into-ims

# refresh pattern for debs and udebs
refresh_pattern deb$   129600 100% 129600 reload-into-ims
refresh_pattern udeb$   129600 100% 129600 reload-into-ims
refresh_pattern tar.gz$  129600 100% 129600 reload-into-ims
refresh_pattern ova$ 329600 100% 329600 reload-into-ims
refresh_pattern tar.xz$  129600 100% 129600 reload-into-ims
refresh_pattern tar.bz2$  129600 100% 129600 reload-into-ims

# always refresh Packages and Release files
refresh_pattern \/(Packages|Sources)(|\.bz2|\.gz|\.xz)$ 0 0% 0 refresh-ims
refresh_pattern \/Release(|\.gpg)$ 0 0% 0 refresh-ims
refresh_pattern \/InRelease$ 0 0% 0 refresh-ims
refresh_pattern \/(Translation-.*)(|\.bz2|\.gz|\.xz)$ 0 0% 0 refresh-ims

# handle meta-release and changelogs.ubuntu.com special
refresh_pattern changelogs.ubuntu.com/*  0  1% 1


#acl QUERY urlpath_regex cgi-bin \?
#cache deny QUERY

refresh_pattern ^ftp:           1440    20%     10080
refresh_pattern ^gopher:        1440    0%      1440
#refresh_pattern -i (/cgi-bin/|\?) 0     0%      0
refresh_pattern -i cgi-bin        0       0%      0
refresh_pattern .               0       20%     4320

# Network ACL

acl localnet src 192.168.0.0/16 # RFC 1918 possible internal network
acl localnet src fc00::/7       # RFC 4193 local private network range
acl localnet src fe80::/10      # RFC 4291 link-local (directly plugged) machines

# Port ACL

acl SSL_ports port 443          # https
acl SSL_ports port 563          # snews
acl SSL_ports port 873          # rync
acl Safe_ports port 80 8080     # http
acl Safe_ports port 21          # ftp
acl Safe_ports port 443 563     # https
acl Safe_ports port 70          # gopher
acl Safe_ports port 210         # wais
acl Safe_ports port 1025-65535  # unregistered ports
acl Safe_ports port 280         # http-mgmt
acl Safe_ports port 488         # gss-http
acl Safe_ports port 591         # filemaker
acl Safe_ports port 777         # multiling http
acl Safe_ports port 5938	# teamviewer
acl Safe_ports port 6000	# skype
acl purge method PURGE
acl CONNECT method CONNECT

#acl forbidden dstdomain "/etc/squid/forbidden_domains"
acl blockfiles urlpath_regex "/etc/squid/blocks.files.acl"


#request_header_add X-GoogApps-Allowed-Domains "sphererays.net" all

deny_info ERR_BLOCKED_FILES blockfiles
#http_access deny blockfiles

#http_access deny forbidden

# Access Restrictions

acl BlockedHost src  192.168.1.124 192.168.1.125 192.168.1.168 192.168.1.83
http_access deny BlockedHost

http_access allow manager localhost localnet
http_access deny manager

http_access allow purge localhost
http_access deny purge

http_access deny !Safe_ports
http_access deny CONNECT !SSL_ports

http_reply_access allow all
htcp_access deny all
icp_access allow all
always_direct allow all



# Response Headers Spoofing

#reply_header_access Via deny all
#reply_header_access X-Cache deny all
#reply_header_access X-Cache-Lookup deny all


store_id_program /home/wall/store-id.pl /home/wall/store_db
store_id_children 5 startup=1



shutdown_lifetime 5 second



#debug_options ALL,2


#loadable_modules /usr/local/lib/ecap_adapter_modifying.so
#ecap_enable on
#ecap_service ecapModifier respmod_precache \
#        uri=ecap://e-cap.org/ecap/services/sample/modifying \
#        victim=the \
#        replacement=a
#adaptation_access ecapModifier allow all

strip_query_terms off
#url_rewrite_program /usr/bin/squidGuard

dns_v4_first on


acl bypass dstdomain .client-channel.google.com
always_direct allow bypass
cache deny bypass

acl bypass1 dstdomain .clients6.google.com
always_direct allow bypass1
cache deny bypass1
dns_nameservers 8.8.8.8 8.8.4.4


