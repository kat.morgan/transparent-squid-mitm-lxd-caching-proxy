apk update
apk add squid ca-certificates vim tcpdump libressl openssl bash tree mini_httpd
echo "proxy.mini-stack.maas" >/etc/hostname

mkdir /root/ca
mkdir /root/certs
mkdir /etc/squid/filter

wget -O /etc/network/interfaces https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/config/interfaces
wget -O /etc/mini_httpd/mini_httpd.conf https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/config/mini_httpd.conf
wget -O /etc/squid/filter/ad_block.urls https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/config/filter/ad_block.urls
wget -O /etc/squid/filter/ad_block.regex https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/config/filter/ad_block.regex
wget -O /etc/squid/squid.conf https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/config/squid.conf
wget -O /root/ca/openssl.cnf https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/ca/openssl.cnf
wget -O /root/ca/init-pki https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/ca/init-pki

chmod +x /root/ca/init-pki 

. /root/ca/init-pki 
chmod 400 /root/certs/*
chown squid /root/certs/*

ln -f /root/certs/squid-ca.crt /var/www/localhost/htdocs/

squid -z ; echo ""
rm -rf /var/cache/squid/ssl_db; /usr/lib/squid/security_file_certgen -c -s /var/cache/squid/ssl_db -M 4
chown squid:squid /var/cache/squid -R

rc-update add mini_httpd
rc-update add squid
reboot
