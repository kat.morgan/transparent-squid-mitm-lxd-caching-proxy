# Transparent Squid MITM SSL_BUMP Alpine LXD Caching Proxy
#### NOTICE: Container built with hard-coded host/domain/tld & IP configs for [CCIO Mini-Stack](https://github.com/KathrynMorgan/mini-stack)    

LXD mitm Local network lab caching WAN accelerator proxy 

WARNING: this lab tool has moral and legal implications and is not intended to be used for nefarious purposes or to exploit real world security measures. 

# Build Server:
#### Create the LXD Squid Profile
```sh
wget -O /tmp/profile-lxd-squid.yaml https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/config/profile-lxd-squid.yaml
lxc profile create squid && lxc profile edit squid < <( cat /tmp/profile-lxd-squid.yaml )
```
#### Launch the squid proxy container
```sh
lxc launch images:alpine/3.9/amd64 proxy -p squid && sleep 2 && echo "Created squid proxy container"
```
#### Run the squid build script && cat cache log after container reboot
```sh
lxc exec proxy -- /bin/ash -c "wget -O- https://gitlab.com/kat.morgan/transparent-squid-mitm-lxd-caching-proxy/raw/master/bin/build-squid4.sh | /bin/ash"
lxc exec proxy -- /bin/ash -c "cat /var/log/squid/cache.log"
```

# Configure Ubuntu Client:
#### Import Certificate
```sh
mkdir /usr/local/share/ca-certificates/proxy
wget -P /usr/local/share/ca-certificates/proxy/ http://10.10.0.52:8080/squid-ca.crt
chmod 400 /usr/local/share/ca-certificates/proxy/squid-ca.crt
update-ca-certificates
```
#### Environment proxy
```sh
cat <<EOF >>/etc/profile.d/proxy.sh
export ftp_proxy=http://10.10.0.52:80
export http_proxy=http://10.10.0.52:80
export https_proxy=http://10.10.0.52:443
EOF
chmod +x /etc/profile.d/proxy.sh
source /etc/profile.d/proxy.sh
```
#### Apt proxy
```sh
cat <<EOF >>/etc/apt/apt.conf.d/99-proxy
Acquire::http::Proxy "http://10.10.0.52:80/";
Acquire::https::Proxy "http://10.10.0.52:443";
EOF
```
#### Snapd proxy
```sh
sudo snap set core proxy.http=http://10.10.0.52:80
sudo snap set core proxy.https=http://10.10.0.52:443
```
#### Wget Proxy
```sh
cat <<EOF >>/etc/wgetrc
https_proxy = http://10.10.0.52:443
http_proxy = http://10.10.0.52:80
ftp_proxy = http://10.10.0.52:80
EOF
```
#### Curl Proxy
```sh
cat <<EOF >>/etc/skel/.curlrc
no-proxy=127.0.0.1,localhost,10.10.0.0/24
proxy = http://10.10.0.52:443
#proxy-user = "admin:admin"
EOF
cp /etc/skel/.curlrc ~/
```
